<?php

class Author {

    public $id;
    public $firstName;
    public $lastName;
    public $grade;

    public function __construct($id, $firstName, $lastName, $grade) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
    }

    public function __toString() {
        return "Author(id: $this->id, name: $this->firstName $this->lastName, grade: $this->grade)";
    }

}
