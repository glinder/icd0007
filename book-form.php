<?php

function getConnection() {
    $host = 'db.mkalmo.xyz';
    $user = 'glinder';
    $pass = '7a3d';
    $database = 'glinder';

    $address = sprintf('mysql:host=%s;dbname=%s', $host, $database);

    try {
        return new PDO($address,$user,$pass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    } catch (PDOException $e) {
        throw new RuntimeException("can't connect");
    }
}

function editById($id, $title, $author1, $grade, $isRead) {
    $conn = getConnection();
    $stmt = $conn->prepare("update books set title = :title, author_id = :author_id, grade = :grade, isRead = :isRead where id = :id;");

    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':title', urlencode($title));
    $stmt->bindValue(':author_id', intval($author1));
    $stmt->bindValue(':grade', $grade);
    $stmt->bindValue(':isRead', $isRead);
    $stmt->execute();
}

function findById($idToFind) {
    $conn = getConnection();
    $stmt = $conn->prepare("select id, title, author_id, grade, isRead from books where id = :id;");

    $stmt->bindValue(':id', $idToFind);
    $stmt->execute();

    foreach ($stmt as $row) {
        $title = $row["title"];
        $author1 = $row["author_id"];
        $grade = $row["grade"];
        $isRead = $row["isRead"];

        return ["id" => $idToFind,
            "title" => $title,
            "author1" => $author1,
            "grade" => $grade,
            "isRead" => $isRead];

    }


}

function deleteById($id) {
    $conn = getConnection();
    $stmt = $conn->prepare("delete from books where id = :id;");
    $stmt->bindValue(':id', $id);
    $stmt->execute();
}

function addBook($title, $author1, $grade, $isRead)
{
    $conn = getConnection();
    $id = $conn->lastInsertId();
    $stmt = $conn->prepare("insert into books (id, author_id, title, grade, isRead) 
                                     values (:book_id, :author_id, :title, :grade, :isRead)");

    $stmt->bindValue(':book_id', $id);
    $stmt->bindValue(':author_id', intval($author1));
    $stmt->bindValue(':title', urlencode($title));
    $stmt->bindValue(':grade', $grade);
    $stmt->bindValue(':isRead', $isRead);
    $stmt->execute();

}

$book = [];
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_POST['id'];
    $title = $_POST['title'];
    $author1 = $_POST['author1'];
    $gradeValue =  $_POST['grade'] ?? "";
    $isRead = isset($_POST['isRead']) ? "y" : "n";
    $cmd = $_POST['cmd'];
    if (isset($cmd) && $cmd === "book-edit") {
        if (3 <= strlen($title) && strlen($title) <= 23) {
            if (isset($_POST['submitButton'])) {
                editById(intval($id), $title, $author1, $gradeValue, $isRead);
                header("Location: index.php?message=updated");
            } elseif (isset($_POST['deleteButton'])) {
                deleteById(intval($id));
                header("Location: index.php?message=deleted");
            }
        } else {
            header(sprintf("Location: book-form.php?id=%s&message=wrongInput&cmd=book-edit", $_POST["id"]));
        }
    } else {
        if (3 <= strlen($title) && strlen($title) <= 23) {
            if (isset($_POST['submitButton'])) {
                addBook($title, $author1, $gradeValue, $isRead);
                header("Location: index.php?message=saved");
            }
        } else {
            header("Location: book-form.php?message=wrongInput");
        }
    }
} else {
    $cmd = $_GET['cmd'] ?? null;
    if (isset($cmd)) {
        $id = $_GET['id'];
        $book = findById(intval($id));
    }

}

?>



<!DOCTYPE html>
    <html lang="et">
        <head>
            <meta charset="utf-8">
            <link rel="stylesheet" type="text/css" href="styles.css">
            <title>Harjutustund 1</title>
        </head>
        <body>

        <nav class="header">
            <a href="index.php" id="book-list-link">Raamatud</a>
            <span>|</span>
            <a href="book-form.php" id="book-form-link">Lisa raamat</a>
            <span>|</span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span>|</span>
            <a href="author-form.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>
        <form id="input-form" action="book-form.php" method="post"><table>
            <?php if (isset($_GET['message']) && $_GET['message'] === 'wrongInput'): ?>
                <div id="error-block"><?php print('Pealkiri peab olema 3 kuni 23 märki!') ?></div>
            <?php endif ?>

            <input type="hidden" name="id" value="{{ $book->id }}">
            <input type="hidden" name="cmd" value="<?= $_GET['cmd']?>">

            <tr>
                <th class="label-cell">Pealkiri:</th>
                <td class="input-cell"><label><input type="text" name="title" value="<?= $cmd ? urldecode($book["title"]) : ($title ?? "") ?>"></label> <br></td>
            </tr>
            <tr>
                <th class="label-cell">Autor:</th>
                <td class="input-cell"><label>
                    <select id="author1" name="author1">
                        <option value=""></option>
                        <?php $conn = getConnection() ?>
                        <?php $stmt = $conn->prepare('SELECT id, firstName, lastName FROM authors'); ?>
                        <?php $stmt->execute() ?>
                        <?php foreach ($stmt as $row): ?>
                            <option value="<?= $row['id'] ?>" <?php if(isset($book['author1']) && $row['id'] === $book['author1']): ?>selected="selected"<?php endif; ?> ><?= urldecode($row['firstName']) . ' ' . urldecode($row['lastName']) ?></option>
                        <?php endforeach; ?>
                    </select>
                </label> <br></td>
            </tr>
            <tr>
                <th class="label-cell">Hinne:</th>
                <td class="input-cell">
                    <tpl tpl-foreach="range(1, 5) as $grade">
                        <label for="grade{{ $grade }}">{{ $grade }}</label>
                        <input type="radio"
                               id="grade{{  $grade  }}"
                               name="grade"
                               tpl-checked="$cmd && $grade === $book->grade"
                               value="{{  $grade  }}">
                    </tpl>
                </td>
            </tr>
            <tr>
                <th class="label-cell"><label>Loetud:</label></th>
                <td class="input-cell"><label><input type="checkbox" name="isRead" <?php if (isset($book["isRead"]) && $book["isRead"] === "y"): ?>checked<?php endif ?>></label></td>
            </tr>
            <tr>
                <th></th>
                <td class="input-cell button">
                    <label><input type="submit" name="submitButton" value="Salvesta"></label>
                    <label><input type="submit" name="deleteButton" value="Kustuta" tpl-if="$cmd"></label>

                </td>
            </tr>

        </table></form>
</main>

<footer>ICD0007 Näidisrakendus</footer>

</body>
</html>