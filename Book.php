<?php

class Book {

    public $id;
    public $title;
    public $author_id;
    public $author;
    public $grade;
    public $isRead;

    public function __construct($id, $title, $author_id, $grade, $isRead) {
        $this->id = $id;
        $this->title = $title;
        $this->author_id = $author_id;
        $this->grade = $grade;
        $this->isRead = $isRead;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function __toString() {
        return "Book(id: $this->id, title: $this->title, author: $this->author, grade: $this->grade, $this->isRead)";
    }


}
