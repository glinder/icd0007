<?php
require_once 'Dao.php';
require_once 'Request.php';
require_once 'tpl.php';

$request = new Request($_REQUEST);
$dao = new Dao();
$authors = $dao->getAuthorList();
$books = $dao->getBookList();
$cmd = $request->param('cmd') ? $request->param('cmd') : 'book-list';
//print $request;

$header1Authors = 'Eesnimi';
$header2Authors = 'Perenimi';
$header1Books = 'Pealkiri';
$header2Books = 'Autor';

if ($cmd === 'book-form') {
    $data = [
        'template' => 'form.html',
        'cmd' => 'book-submit',
        'authors' => $authors,
        'firstCell' => $header1Books,
        'secondCell' => $header2Books,
        'isBookForm' => true
    ];
    print renderTemplate('main.html', $data);


} else if ($cmd === 'author-form') {
    $data = [
        'template' => 'form.html',
        'cmd' => 'author-submit',
        'firstCell' => $header1Authors,
        'secondCell' => $header2Authors,
        'isAuthorForm' => true
    ];
    print renderTemplate('main.html', $data);




} else if ($cmd === 'book-edit') {
        $id = $request->param('id');
        $book = $dao->findBookById($id);
        $data = [
            'firstCell' => $header1Books,
            'secondCell' => $header2Books,
            'template' => 'form.html',
            'cmd' => 'book-submit',
            'isEditForm' => true,
            'isBookForm' => true,
            'title' => $book->title,
            'author1' => $book->author_id,
            'authors' => $authors,
            'id' => $book->id,
            'gradevalue' => $book->grade
        ];
        print renderTemplate('main.html', $data);

} else if ($cmd === 'author-edit') {
    $id = $request->param('id');
    $author = $dao->findAuthorById($id);
    $data = [
        'firstCell' => $header1Authors,
        'secondCell' => $header2Authors,
        'firstName' => $author->firstName,
        'lastName' => $author->lastName,
        'gradevalue' => $author->grade,
        'template' => 'form.html',
        'cmd' => 'author-submit',
        'isEditForm' => true,
        'isAuthorForm' => true,
        'id' => $id
    ];
    print renderTemplate('main.html', $data);




} else if ($cmd === 'book-submit') {
    $deleteButton = $request->param('deleteButton');
    $submitButton = $request->param('submitButton');
    $id = $request->param('id');
    if ($deleteButton) {
        $dao->deleteBookById($id);
        $books = $dao->getBookList();
        $message = 'deleted';
        header("Location: ?cmd=book-list&message=$message");

    } else if ($submitButton) {
        $title = $request->param('title');
        $author = $request->param('author1');
        $grade = $request->param('grade');
        $isRead = $request->param('isRead');
        $errors = validateBook($title);
        if (empty($errors)) {
            if (!empty($id)) {
                $dao->editBookById($id, $title, $author, $grade, $isRead);
                $message = "muudetud";
            } else {
                $dao->addBook($title, $author, $grade, $isRead);
                $message = 'lisatud';
            }
            $books = $dao->getBookList();
            header("Location: ?cmd=book-list&message=$message");

        } else {
            $data = [
                'firstCell' => $header1Books,
                'secondCell' => $header2Books,
                'errors' => $errors,
                'title' => $title,
                'author' => $author,
                'gradevalue' => $grade,
                'isReadValue' => $isRead,
                'isBookForm' => true,
                'authors' => $authors,
                'template' => 'form.html',
                'cmd' => 'book-submit'
            ];
            print renderTemplate('main.html', $data);
        }
    }


} else if ($cmd === 'author-submit') {
    $deleteButton = $request->param('deleteButton');
    $submitButton = $request->param('submitButton');
    $id = $request->param('id');
    if ($deleteButton) {
        $dao->deleteAuthorById($id);
        $authors = $dao->getAuthorList();
        $message = 'kustutatud';
        header("Location: ?cmd=author-list&message=$message");

    } else if ($submitButton) {
        $firstName = $request->param('firstName');
        $lastName = $request->param('lastName');
        $grade = $request->param('grade');
        $errors = validateAuthor($firstName, $lastName);
        if (empty($errors)) {
            if (!empty($id)) {
                $dao->editAuthorById($id, $firstName, $lastName, $grade);
                $message = "muudetud";
            } else {
                $dao->addAuthor($firstName, $lastName, $grade);
                $message = 'lisatud';
            }
            $authors = $dao->getAuthorList();
            header("Location: ?cmd=author-list&message=$message");

        } else {
            $data = [
                'firstCell' => $header1Authors,
                'secondCell' => $header2Authors,
                'errors' => $errors,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'gradevalue' => $grade,
                'isAuthorForm' => true,
                'template' => 'form.html',
                'cmd' => 'author-submit'
            ];
            print renderTemplate('main.html', $data);
        }
    }





} else if ($cmd === 'book-list') {
    $data = [
        'template' => 'list.html',
        'cmd' => 'book-list',
        'header1' => $header1Books,
        'header2' => $header2Books,
        'books' => $books
    ];
    print renderTemplate('main.html', $data);

} else if ($cmd === 'author-list') {
    $data = [
        'template' => 'list.html',
        'cmd' => 'author-list',
        'header1'=> $header1Authors,
        'header2' => $header2Authors,
        'authors' => $authors
    ];
    print renderTemplate('main.html', $data);
}

function validateAuthor($input1, $input2) {
    if (1 <= strlen($input1) && strlen($input1) <= 21) {
        if (2 <= strlen($input2) && strlen($input2) <= 22) {
            return [];
        }
        return ['Perenimi peab olema 2 kuni 22 märki!'];
    }
    return ['Eesnimi peab olema 1 kuni 21 märki!'];
}

function validateBook($input) {
    if (3 <= strlen($input) && strlen($input) <= 23) {
        return [];
    }
    return ['Pealkiri peab olema 3 kuni 23 märki!'];
}