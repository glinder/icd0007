<?php

function getConnection() {
    $host = 'db.mkalmo.xyz';
    $user = 'glinder';
    $pass = '7a3d';
    $database = 'glinder';

    $address = sprintf('mysql:host=%s;dbname=%s', $host, $database);

    try {
        return new PDO($address,$user,$pass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    } catch (PDOException $e) {
        throw new RuntimeException("can't connect");
    }
}

function editById($id, $firstName, $lastName, $grade) {
    $conn = getConnection();
    $stmt = $conn->prepare("update authors set firstName = :firstName, lastName = :lastName, grade = :grade where id = :id;");

    $stmt->bindValue(':id', $id);
    $stmt->bindValue(':firstName', urlencode($firstName));
    $stmt->bindValue(':lastName', urlencode($lastName));
    $stmt->bindValue(':grade', $grade);
    $stmt->execute();
}

function findById($idToFind) {
    $conn = getConnection();
    $stmt = $conn->prepare("select firstName, lastName, grade from authors where id = :id;");

    $stmt->bindValue(':id', $idToFind);
    $stmt->execute();

    foreach ($stmt as $row) {
        $firstName = $row["firstName"];
        $lastName = $row["lastName"];
        $grade = $row["grade"];
        return ["id" => $idToFind,
            "firstName" => $firstName,
            "lastName" => $lastName,
            "grade" => $grade];
    }
}

function deleteById($id) {
    $conn = getConnection();
    $stmt = $conn->prepare("delete from authors where id = :id;");
    $stmt->bindValue(':id', $id);
    $stmt->execute();
}

function addAuthor($firstName, $lastName, $grade) {
    $conn = getConnection();
    $id = $conn->lastInsertId();
    $stmt = $conn->prepare("insert into authors (id, firstName, lastName, grade) 
                                     values (:author_id, :firstName, :lastName, :grade)");

    $stmt->bindValue(':author_id', $id);
    $stmt->bindValue(':firstName', urlencode($firstName));
    $stmt->bindValue(':lastName', urlencode($lastName));
    $stmt->bindValue(':grade', $grade);
    $stmt->execute();
}


$author = [];
$message = "";
if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $id = $_POST["id"];
    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $grade =  $_POST["grade"];
    $cmd = $_POST["cmd"];
    if (isset($cmd) && $cmd === "author-edit") {
        if (1 <= strlen($firstName) && strlen($firstName) <= 21) {
            if (2 <= strlen($lastName) && strlen($lastName) <= 22) {
                if (isset($_POST['submitButton'])) {
                    editById(intval($id), $firstName, $lastName, $grade);
                    header("Location: author-list.php?message=updated");
                } elseif (isset($_POST['deleteButton'])) {
                    deleteById(intval($id));
                    header("Location: author-list.php?message=deleted");
                }
            } else {
                header(sprintf("Location: author-form.php?id=%s&message=wrongInputL&cmd=author-edit", $_POST["id"]));
            }
        } else {
            header(sprintf("Location: author-form.php?id=%s&message=wrongInputF&cmd=author-edit", $_POST["id"]));
        }
    } else {
        if (1 <= strlen($firstName) && strlen($firstName) <= 21) {
            if (2 <= strlen($lastName) && strlen($lastName) <= 22) {
                if (isset($_POST['submitButton'])) {
                    addAuthor($firstName, $lastName, $grade);
                    header("Location: author-list.php?message=saved");
                }

            } else {
                header("Location: author-form.php?message=wrongInputL");
            }
        } else {
            header(sprintf("Location: author-form.php?message=wrongInputF"));
        }
    }

} else {
    $cmd = $_GET["cmd"] ?? null;
    if (isset($cmd)) {
        $id = $_GET["id"];
        $author = findById(intval($id));
    }
}

?>



<!DOCTYPE html>
<html lang="et">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title>Harjutustund 1</title>
    </head>
    <body>

    <nav class="header">
        <a href="index.php" id="book-list-link">Raamatud</a>
        <span>|</span>
        <a href="book-form.php" id="book-form-link">Lisa raamat</a>
        <span>|</span>
        <a href="author-list.php" id="author-list-link">Autorid</a>
        <span>|</span>
        <a href="author-form.php" id="author-form-link">Lisa autor</a>
    </nav>

    <main>
    <form id="input-form" action="author-form.php" method="post"><table>

        <?php if (isset($_GET["message"]) && $_GET["message"] === "wrongInputF"): ?>
            <div id="error-block"><?php print("Eesnimi peab olema 1 kuni 21 märki!") ?></div>
        <?php endif ?>
        <?php if (isset($_GET["message"]) && $_GET["message"] === "wrongInputL"): ?>
            <div id="error-block"><?php print("Perenimi peab olema 2 kuni 22 märki!") ?></div>
        <?php endif ?>

        <input type="hidden" name="id" value="<?= $author["id"]?>">
        <input type="hidden" name="cmd" value="<?= $_GET["cmd"]?>">

        <tr>
            <th class="label-cell">Eesnimi:</th>
            <td class="input-cell"><label><input type="text" id="firstName" name="firstName" value="<?= $cmd ? urldecode($author["firstName"]) : ($firstName ?? "") ?>"></label> <br></td>
        </tr>
        <tr>
            <th class="label-cell">Perekonnanimi:</th>
            <td class="input-cell"><label><input type="text" id="lastName" name="lastName" value="<?= $cmd ? urldecode($author["lastName"]) : ($lastName ?? "") ?>"></label> <br></td>
        </tr>
        <tr>
            <th class="label-cell">Hinne:</th>
            <td class="input-cell">
                <?php foreach (range(1, 5) as $grade): ?>
                    <label for="grade<?= $grade ?>"><?= $grade ?></label>
                    <input type="radio"
                           id="grade<?= $grade ?>"
                           name="grade"
                        <?= $cmd && $grade === intval($author["grade"]) ? 'checked' : '' ?>
                           value="<?= $grade ?>">
                <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <th></th>
            <td class="input-cell button">
                <label><input type="submit" name="submitButton" value="Salvesta"></label>
                <?php if (isset($cmd)): ?>
                <label><input type="submit" name="deleteButton" value="Kustuta" class="delete"></label>
                <?php endif; ?>
            </td>
        </tr>
    </table></form>
</main>

<footer>ICD0007 Näidisrakendus</footer>

</body>
</html>