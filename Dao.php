<?php
require_once 'Author.php';
require_once 'Book.php';


class Dao {


    public function __construct() {

    }

    function getConnection() {
        $host = 'db.mkalmo.xyz';
        $user = 'glinder';
        $pass = '7a3d';
        $database = 'glinder';

        $address = sprintf('mysql:host=%s;dbname=%s', $host, $database);

        try {
            return new PDO($address,$user,$pass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        } catch (PDOException $e) {
            throw new RuntimeException("can't connect");
        }
    }

    function getBookList() {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("select id, title, author_id, grade, isRead from books;");
        $bookList = [];
        $stmt->execute();
        foreach ($stmt as $row) {
            array_push($bookList, new Book($row['id'], urldecode($row['title']), $row['author_id'], $row['grade'], $row['isRead']));
        }
        $authorList = $this->getAuthorList();
        foreach ($bookList as $book) {
            foreach ($authorList as $author) {
                if (intval($book->author_id) === intval($author->id)) {
                    $book->setAuthor($author);
                }
            }
        }
        return $bookList;
}

    function getAuthorList() {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("select id, firstName, lastName, grade from authors;");
        $list = [];
        $stmt->execute();
        foreach ($stmt as $row) {
            array_push($list, new Author($row['id'], urldecode($row['firstName']), urldecode($row['lastName']), $row['grade']));
        }
        return $list;
    }

    function editBookById($id, $title, $author1, $grade, $isRead) {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("update books set title = :title, author_id = :author_id, grade = :grade, isRead = :isRead where id = :id;");

        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':title', urlencode($title));
        $stmt->bindValue(':author_id', intval($author1));
        $stmt->bindValue(':grade', $grade);
        $stmt->bindValue(':isRead', $isRead);
        $stmt->execute();
    }

    function findBookById($idToFind) {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("select id, title, author_id, grade, isRead from books where id = :id;");

        $stmt->bindValue(':id', $idToFind);
        $stmt->execute();

        foreach ($stmt as $row) {
            $title = $row["title"];
            $author_id = $row["author_id"];
            $grade = $row["grade"];
            $isRead = $row["isRead"];

            $book = new Book($idToFind, $title, $author_id, $grade, $isRead);
            $book->setAuthor($this->findAuthorById($author_id));
            return $book;

        }


    }

    function deleteBookById($id) {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("delete from books where id = :id;");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }

    function addBook($title, $author1, $grade, $isRead)
    {
        $conn = $this->getConnection();
        $id = $conn->lastInsertId();
        $stmt = $conn->prepare("insert into books (id, author_id, title, grade, isRead) 
                                     values (:book_id, :author_id, :title, :grade, :isRead)");

        $stmt->bindValue(':book_id', $id);
        $stmt->bindValue(':author_id', intval($author1));
        $stmt->bindValue(':title', urlencode($title));
        $stmt->bindValue(':grade', $grade);
        $stmt->bindValue(':isRead', $isRead);
        $stmt->execute();

    }

    function editAuthorById($id, $firstName, $lastName, $grade) {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("update authors set firstName = :firstName, lastName = :lastName, grade = :grade where id = :id;");

        $stmt->bindValue(':id', $id);
        $stmt->bindValue(':firstName', urlencode($firstName));
        $stmt->bindValue(':lastName', urlencode($lastName));
        $stmt->bindValue(':grade', $grade);
        $stmt->execute();
    }

    function findAuthorById($idToFind) {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("select firstName, lastName, grade from authors where id = :id;");

        $stmt->bindValue(':id', $idToFind);
        $stmt->execute();

        foreach ($stmt as $row) {
            $firstName = $row["firstName"];
            $lastName = $row["lastName"];
            $grade = $row["grade"];
            return new Author($idToFind, $firstName, $lastName, $grade);
        }
    }

    function deleteAuthorById($id) {
        $conn = $this->getConnection();
        $stmt = $conn->prepare("delete from authors where id = :id;");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }

    function addAuthor($firstName, $lastName, $grade) {
        $conn = $this->getConnection();
        $id = $conn->lastInsertId();
        $stmt = $conn->prepare("insert into authors (id, firstName, lastName, grade) 
                                     values (:author_id, :firstName, :lastName, :grade)");

        $stmt->bindValue(':author_id', $id);
        $stmt->bindValue(':firstName', urlencode($firstName));
        $stmt->bindValue(':lastName', urlencode($lastName));
        $stmt->bindValue(':grade', $grade);
        $stmt->execute();
    }


}
