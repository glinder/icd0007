<?php
$host = 'db.mkalmo.xyz';
const USERNAME = 'glinder';
const PASSWORD = '7a3d';
$database = 'glinder';
$address = sprintf('mysql:host=%s;dbname=%s', $host, $database);

$connection = new PDO($address, USERNAME, PASSWORD,
    [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

try {
    $connection = new PDO($address, USERNAME, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
} catch (PDOException $e) {
    throw new RuntimeException("can't connect");
}
?>


<!DOCTYPE html>
   <html lang="et">
       <head>
           <meta charset="utf-8">
           <link rel="stylesheet" type="text/css" href="styles.css">
           <title>Harjutustund 1</title>
       </head>
       <body>


        <nav class="header">
            <a href="index.php" id="book-list-link">Raamatud</a>
            <span>|</span>
            <a href="book-form.php" id="book-form-link">Lisa raamat</a>
            <span>|</span>
            <a href="author-list.php" id="author-list-link">Autorid</a>
            <span>|</span>
            <a href="author-form.php" id="author-form-link">Lisa autor</a>
        </nav>

        <main>
            <table>

                <?php if (isset($_GET['message'])): ?>
                    <?php if ($_GET['message'] == "updated"): ?>
                        <div id="message-block"><?php print "Muudetud!" ?></div>
                    <?php elseif ($_GET['message'] == "saved"): ?>
                        <div id="message-block"><?php print "Lisatud!" ?></div>
                    <?php elseif ($_GET['message'] == "deleted"): ?>
                        <div id="message-block"><?php print "Kustutatud!" ?></div>
                    <?php endif ?>
                <?php endif ?>
                <tr>
                    <th class="header1">Eesnimi</th>
                    <th class="header2">Perekonnanimi</th>
                    <th class="header3">Hinne</th>
                </tr>

                <?php $stmt = $connection->prepare('SELECT * FROM authors'); ?>
                <?php $stmt->execute() ?>
                <?php foreach ($stmt as $row): ?>
                <tr>
                    <td><a href="author-form.php?id=<?= $row["id"] ?>&cmd=author-edit"><?php echo sprintf("%s", urldecode($row["firstName"])) ?></a></td>
                    <td><?php echo sprintf("%s", urldecode($row["lastName"])) ?></td>
                    <td><?php print ($row["grade"] === "") ? sprintf("") : sprintf("%s/5", $row["grade"]) ?></td>
                </tr>
                <?php endforeach; ?>
            </table>
        </main>

        <footer>ICD0007 Näidisrakendus</footer>

       </body>
   </html>